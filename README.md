Web de la Fàbrica de la transició

# Modificar contingut del web
Per modificar el contingut del web fer les següents passes:
- Obre l'editor web a la carpeta `/contents/`: https://gitlab.com/-/ide/project/la_fabrica_de_la_transicio/web-hugo/tree/master/-/content/
- modifica els arxius de text que tenen el contingut (son arxius amb extensió `.md` o format Markdown). Recorda canviar el contingut a tots els idiomes, cadascun està a una carpeta: `ca/`, `es/`, `en/`
- ara pots premer el botó blau `Commit` i s'obrira un petit formulari
- al campt de text `Commit message` fes una petita explicació de qué estás cambiant. Escull la opció `Commit to Master branch`. Finalment prem el botó `Commit`
- Espera uns minuts i accedeix a https://zen-swanson-f5040f.netlify.app. Comprova que els teus canvis apareixen al web.

# TASQUES PENDENTS
Llistat de tasques pendents fins tenir la versió 1.0
- [x] posar el menú d'idiomes a peu de pàgina
- [x] fer que el contingut traduit no estigui en carpetes sino directament a la carpeta content
- [x] afegir tots els menus que hi han al miró
- [ ] cambiar l'alineació del text de centrat (actual) a esquerra (desitjat)
- [ ] cambiar el menú per un menú desplegable tipus telèfon mòbil (per exemple https://toha-guides.netlify.app, veure https://github.com/dwyl/learn-tachyons#responsive-navigation-menu)
- [ ] veure com afegir imatges als articles
- [ ] afegir SEF urls per a cada idioma. Ara totes les urls son en catala. Mirar https://discourse.gohugo.io/t/solved-reference-multilingual-content-from-template/9461/19
- [ ] ...
