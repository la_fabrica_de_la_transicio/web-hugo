---
title: "project"
featured_image: '/images/projecte.jpg'
description: ""
---

This is a testing text

1. Un lloc per viure i per experimentar la sostenibilitat, amb més de 4.000 m2 d’espai habitable i més de 1.000 m2 d’espais comuns.
2. Un lloc amb horta al voltant on sigui possible conrear el nostre propi menjar orgànic.
3. Un lloc on produir la nostra pròpia energia renovable.
...