---
title: "Transition factory"
featured_image: '/images/inici.jpg'
description: "Reviving our villages, inspiring our cities"
---
**Once upon a time...**


Provant provant Javi i Laiaaa

Once upon a time, in a small village of Catalonia, **El Catllar** (near Tarragona), an old and abandoned paper and textile factory built in 1754 was rescued from collapsing and disappearing for ever. In this historical patrimony, a new type of “experimental textile” was being woven.

While society was facing many simultaneous crisis—climate, environmental, financial, social and political—people living at this factory aimed at looking at the problem from another angle: **rethinking** life at the village level, in complementarity to what was being done at the city level.

This “Once upon a time” will soon be a **reality** in El Catllar with the La **Fàbrica de la Transició** cooperative. In summer 2020 we received the Administration’s consent to go ahead with our co-housing project in one of the three buildings. The next step is to find entities or organizations (cooperatives, companies, trusts, foundations…) that share our vision and want to join us by taking over of the two remaining buildings (with a total area of approximately 3,000 m2).

