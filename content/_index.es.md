---
title: La Fàbrica de la Transición
featured_image: "/images/inici.jpg"
description: Reviviendo a nuestros pueblos, inspirando a nuestras ciudades.

---
**Hi havia una vegada...**

Provant provant Javi i Laiaaaa

Érase una vez, en un pequeño pueblo de la comarca del Tarragonès (Cataluña), **El Catllar**, una fábrica de papel y de tejidos construida en 1754 junto a un viejo molino medieval. Los intensos cambios del siglo XX provocaron su abandono pero, por suerte, fue rescatada antes de hundirse y desaparecer para siempre. Y en ese espacio de patrimonio empezó a florecer un nuevo tejido, un “tejido social”.

Mientras la sociedad se debatía entre crisis globales —climática, sanitaria, ambiental, financiera, social y política—, la gente que empezó a habitar la antigua fábrica se propuso afrontar los problemas desde un nuevo ángulo: **repensar** la vida desde las pequeñas comunidades y en relación con las poblaciones de alrededor.

Ese “Érase una vez” será pronto una **realidad** en El Catllar con la cooperativa **La Fàbrica de la Transició**. En verano de 2020 hemos recibido la conformidad de la Administración para poder llevar adelante nuestro proyecto de covivienda en uno de los tres edificios. El pas siguiente es encontrar entidades u organizaciones (cooperativas, empresas, patronatos, fundaciones…) que estén alineadas con nuestra visión y que quieran unirse a nosotros ocupando uno de los dos edificios restantes (con una superficie total de 3.000 m2, aproximadamente).