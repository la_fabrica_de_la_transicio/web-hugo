---
title: "La Fàbrica de la Transició"
featured_image: '/images/inici.jpg'
description: "Revivint els nostres pobles, inspirant les nostres ciutats"
weight: 1
---
**Hi havia una vegada...**

Hola hola provant i trastejant el Javi i la Laiaaaa

Hi havia una vegada, en un petit poble del Tarragonès, **el Catllar**, una fàbrica de paper i de teixits construïda el 1754 al costat d’un vell molí medieval. Els intensos canvis del segle XX van provocar el seu abandonament però, per sort, va ser rescatada abans d’enfonsar-se i desaparèixer per sempre. I en aquest espai de patrimoni va començar a florir un nou teixit, un “teixit social”.

Mentre la societat es debatia entre crisis globals —climàtica, sanitària, ambiental, financera, social i política—, la gent que va començar a habitar l’antiga fàbrica es va proposar afrontar els problemes des d’un nou angle: **repensar** la vida des de les petites comunitats i en relació amb les poblacions del voltant.

Aquest “Hi havia una vegada…” serà aviat una **realitat** al Catllar amb la cooperativa **La Fàbrica de la Transició**. L’estiu del 2020 vam rebre el vistiplau de l’Administració per poder tirar endavant el nostre projecte de cohabitatge en un dels tres edificis.

El pas següent és trobar entitats o organitzacions (cooperatives, empreses, patronats, fundacions…) que estiguin alineades amb la nostra visió i que vulguin unir-se a nosaltres ocupant un dels dos edificis restants (amb una superfície total de 3.000 m2, aproximadament).

